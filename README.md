# Automated Twitter Bot

Fetches tweets from popular Twitter accounts and combines them to generate new ones. Leverages [GitLab's CI/CD](https://docs.gitlab.com/ee/ci) pipeline to automatically generate and post tweets daily.

## Resources
- [How to Scrape Tweets With snscrape](https://betterprogramming.pub/how-to-scrape-tweets-with-snscrape-90124ed006af)
- [Python implementation of a Markov Text Generator](https://github.com/codebox/markov-text)
- [Random Sentence Generator](https://github.com/ddycai/random-sentence-generator)
- [Using a Markov chain sentence generator in Python](https://towardsdatascience.com/using-a-markov-chain-sentence-generator-in-python-to-generate-real-fake-news-e9c904e967e)
- [DeepAI Text Generation API](https://deepai.org/machine-learning-model/text-generator)
