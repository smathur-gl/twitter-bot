import tweepy
import os

def set_up():
	api = tweepy.Client(consumer_key=os.environ['CONSUMER_KEY'],
        consumer_secret=os.environ['CONSUMER_SECRET'],
        access_token=os.environ['ACCESS_TOKEN'],
        access_token_secret=os.environ['ACCESS_SECRET'])
	return api

def run(generated_tweets):
	api = set_up()
	for tweet in generated_tweets:
		if len(tweet) >= 15 and len(tweet) <= 280:
			client.create_tweet(text=tweet)
			break
